### Build android
- ./gradlew build
- or download [APK](https://bitbucket.org/tttzof351/coin/downloads/app-debug.apk)

### Build/run server
- Dowload rust and crago last version (>= 1.56)
- cd server/
- cargo build
- cargo run <your_ipv4_address>

### Models
- [PlayerId](https://bitbucket.org/tttzof351/coin/src/47d51efcb29cc0c952408ec23c974d19f981955d/android/app/src/main/java/com/tttzof351/coingame/models/models.kt#lines-38) - after connection client receives player_id
- [StartGame](https://bitbucket.org/tttzof351/coin/src/47d51efcb29cc0c952408ec23c974d19f981955d/android/app/src/main/java/com/tttzof351/coingame/models/models.kt#lines-134) - client sends this message to init game for all connected users
- [GameParams](https://bitbucket.org/tttzof351/coin/src/47d51efcb29cc0c952408ec23c974d19f981955d/android/app/src/main/java/com/tttzof351/coingame/models/models.kt#lines-61) - client receives params for start game (after this client starts game UI)
- [ListCoins](https://bitbucket.org/tttzof351/coin/src/47d51efcb29cc0c952408ec23c974d19f981955d/android/app/src/main/java/com/tttzof351/coingame/models/models.kt#lines-150) - client sends/receive this message to explosion coins
- [FinishGame](https://bitbucket.org/tttzof351/coin/src/47d51efcb29cc0c952408ec23c974d19f981955d/android/app/src/main/java/com/tttzof351/coingame/models/models.kt#lines-200) - client receives final scores upon finishing game

### TODO
- Handle reconnection
- OpenGL/Instancing
