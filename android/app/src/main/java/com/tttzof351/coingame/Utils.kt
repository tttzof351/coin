package com.tttzof351.coingame

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.PixelFormat
import android.graphics.PorterDuff
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.os.Looper
import kotlin.math.pow
import android.graphics.BitmapFactory
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.BitmapDrawable
import android.os.Handler
import android.util.Log
import com.tttzof351.coingame.models.Vec2d
import java.io.IOException
import java.io.InputStream
import java.lang.IllegalStateException


const val SIZE_OF_FLOAT = 4

var uiHandler = Handler(Looper.getMainLooper())

fun GLSurfaceView.makeTransparent() {
    this.setZOrderOnTop(true)
    this.setEGLContextClientVersion(2)
    this.setEGLConfigChooser(8, 8, 8, 8, 16, 0)
    this.holder.setFormat(PixelFormat.RGBA_8888)
    //this.holder.setFormat(PixelFormat.TRANSLUCENT)
}

fun loadShader(shaderType: Int, shaderSource: String): Int {
    val handle = GLES20.glCreateShader(shaderType)
    if (handle == GLES20.GL_FALSE) throw RuntimeException("Error creating shader!")

    // set and compile the shader
    GLES20.glShaderSource(handle, shaderSource)
    GLES20.glCompileShader(handle)

    // check if the compilation was OK
    val compileStatus = IntArray(1)
    GLES20.glGetShaderiv(handle, GLES20.GL_COMPILE_STATUS, compileStatus, 0)
    return if (compileStatus[0] == 0) {
        val error = GLES20.glGetShaderInfoLog(handle)
        GLES20.glDeleteShader(handle)
        throw RuntimeException("Error compiling shader: $error")
    } else handle
}

fun loadVertexShader(shaderSource: String) = loadShader(GLES20.GL_VERTEX_SHADER, shaderSource)

fun loadFragmentShader(shaderSource: String) = loadShader(GLES20.GL_FRAGMENT_SHADER, shaderSource)

fun createProgram(vertexShader: Int, fragmentShader: Int): Int {
    val handle = GLES20.glCreateProgram()
    if (handle == GLES20.GL_FALSE) throw RuntimeException("Error creating program!")

    GLES20.glAttachShader(handle, vertexShader)
    GLES20.glAttachShader(handle, fragmentShader)
    GLES20.glLinkProgram(handle)

    val linkStatus = IntArray(1)
    GLES20.glGetProgramiv(handle, GLES20.GL_LINK_STATUS, linkStatus, 0)
    return if (linkStatus[0] == 0) {
        val error = GLES20.glGetProgramInfoLog(handle)
        GLES20.glDeleteProgram(handle)
        throw RuntimeException("Error in program linking: $error")
    } else handle
}

fun createTransparentBitmap(bitmap: Bitmap, opacity: Int): Bitmap {
    val mutableBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true)
    val canvas = Canvas(mutableBitmap)
    val colour = ((opacity and 0xFF) shl  24)
    canvas.drawColor(colour, PorterDuff.Mode.DST_IN);
    return mutableBitmap
}

fun isUiThread(): Boolean {
    return Looper.getMainLooper().thread == Thread.currentThread()
}

fun runOnUiThread(action: Runnable) {
    if (isUiThread()) {
        action.run()
    } else {
        uiHandler.post(action)
    }
}


fun assertUiThread(msg: String) {
    if (!isUiThread()) {
        throw IllegalStateException(msg)
    }
}

fun euclidDistance(point1: Vec2d, point2: Vec2d): Float {
    return kotlin.math.sqrt((point1[0] - point2[0]).pow(2) + (point1[1] - point2[1]).pow(2))
}

fun euclidDistance(vector: Vec2d): Float {
    return kotlin.math.sqrt((vector[0]).pow(2) + (vector[1]).pow(2))
}

fun readBitmapFromAssets(context: Context, filePath: String): Bitmap? {
    val assetManager = context.assets
    val istr: InputStream
    var bitmap: Bitmap? = null
    try {
        istr = assetManager.open(filePath)
        bitmap = BitmapFactory.decodeStream(istr)
    } catch (e: IOException) {
        // handle exception
        Log.d("UTILS", "readBitmapFromAssets", e)
    }

    return bitmap
}

fun createChestAnimation(context: Context): Pair<AnimationDrawable, AnimationDrawable> {
    val bitmaps = Array(4) { index ->
        val path = "chest/inplaced${index}.png"
        readBitmapFromAssets(context, path)!!
    }

    val directAnimation = AnimationDrawable()
    directAnimation.isOneShot = true
    for (bitmap in bitmaps) {
        directAnimation.addFrame(BitmapDrawable(context.resources, bitmap), 100)
    }

    val invertedAnimation = AnimationDrawable()
    invertedAnimation.isOneShot = true
    for (bitmap in bitmaps.reversed()) {
        invertedAnimation.addFrame(BitmapDrawable(context.resources, bitmap), 100)
    }

    return Pair(directAnimation, invertedAnimation)
}

