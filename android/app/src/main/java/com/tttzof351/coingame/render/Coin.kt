package com.tttzof351.coingame.render

import com.tttzof351.coingame.models.GameParams
import com.tttzof351.coingame.models.Vec2d


enum class CoinState {
    NORMAL,
    EXPLOSION
}

class Coin(
    val id: Int,
    var pos: Vec2d,
    var velocity: Vec2d = floatArrayOf(0f, 0f),
    var radius: Float = 150f,
    var checkBound: Boolean = true,
    var state: CoinState = CoinState.NORMAL,
    var startExplosionTimeMs: Long = -1L,
    var explosionFrame: Int = 0
) {
    fun updateState(
        gameParams: GameParams,
        step: Float,
        sceneWidth: Int,
        sceneHeight: Int,
    ) {
        updatePosition(step, sceneWidth, sceneHeight)
        incrementVelocity(gameParams, step)
        incrementRadius(gameParams, step)
    }

    private fun updatePosition(
        step: Float,
        sceneWidth: Int,
        sceneHeight: Int,
    ) {
        this.pos[0] += this.velocity[0] * step
        this.pos[1] += this.velocity[1] * step

        if (checkBound) {
            when {
                (this.pos[0] + radius) > sceneWidth -> {
                    this.velocity[0] = -this.velocity[0]

                    this.pos[0] = sceneWidth - radius
                }
                (this.pos[1] + radius) > sceneHeight -> {
                    this.velocity[1] = -this.velocity[1]

                    this.pos[1] = sceneHeight - radius
                }
                (this.pos[0] - radius) < 0 -> {
                    this.velocity[0] = -this.velocity[0]

                    this.pos[0] = radius
                }
                (this.pos[1] - radius) < 0 -> {
                    this.velocity[1] = -this.velocity[1]

                    this.pos[1] = radius
                }
            }
        }

    }

    private fun incrementVelocity(
        gameParams: GameParams,
        step: Float,
    ) {

        if (this.velocity[0] > gameParams.velocityEnd) {
            this.velocity[0] -= gameParams.velocityIncrement *  (step / QUANTUM_MS)
        }

        if (this.velocity[1] > gameParams.velocityEnd) {
            this.velocity[1] -= gameParams.velocityIncrement *  (step / QUANTUM_MS)
        }
    }

    private fun incrementRadius(
        gameParams: GameParams,
        step: Float,
    ) {
        if (this.radius < gameParams.radiusEnd) {
            this.radius += gameParams.radiusIncrement * (step / QUANTUM_MS)
        }
    }

    fun isFinishedExplosion(frameTimeMs: Long): Boolean {
        if (state != CoinState.EXPLOSION) {
            return false
        }
        return frameTimeMs - startExplosionTimeMs > EXPLOSION_FRAME_DURATION * EXPLOSION_FRAME_COUNT
    }

    fun updateExplosionFrame(frameTimeMs: Long) {
        if (state != CoinState.EXPLOSION) {
            return
        }
        explosionFrame = ((frameTimeMs - startExplosionTimeMs) / EXPLOSION_FRAME_DURATION).toInt()
    }

    fun markExplosion(currentTimeMs: Long) {
        this.state = CoinState.EXPLOSION
        this.checkBound = false
        this.startExplosionTimeMs = currentTimeMs
    }

    companion object {
        const val EXPLOSION_FRAME_DURATION = 24L
        const val EXPLOSION_FRAME_COUNT = 7
        const val QUANTUM_MS = 16L
    }
}