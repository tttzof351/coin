package com.tttzof351.coingame.models

import org.json.JSONArray
import org.json.JSONObject

typealias Vec2d = FloatArray

interface MsgBody {
    val name: String
    fun toJson(): JSONObject
}

class Msg(
    val id: Int,
    val name: String,
    val body: String
) {
    fun toJson(): JSONObject {
        val obj = JSONObject()
        obj.put("id", id)
        obj.put("name", name)
        obj.put("body", body)
        return obj
    }

    companion object {
        fun fromStr(text: String): Msg {
            val obj = JSONObject(text)
            return Msg(
                id = obj.getInt("id"),
                name = obj.getString("name"),
                body = obj.getString("body")
            )
        }
    }
}

class PlayerId(
    val playerId: Int
) : MsgBody {
    override val name: String = NAME

    override fun toJson(): JSONObject {
        val obj = JSONObject()
        obj.put("player_id", playerId)
        return obj
    }

    companion object {
        const val NAME = "player_id"

        fun fromStr(body: String): PlayerId {
            val obj = JSONObject(body)
            return PlayerId(
                playerId = obj.getInt("player_id")
            )
        }
    }
}

class GameParams(
    val gameId: Int,
    val durationMs: Int,
    val generationTimeMs: Int,
    val coinCount: Int,
    val coinBins: Int,
    val radiusStart: Float,
    val radiusEnd: Float,
    val velocityStart: Float,
    val velocityEnd: Float,
    val velocityIncrement: Float,
    val radiusIncrement: Float,
    val seed: Int,
) : MsgBody {
    override val name: String = NAME

    override fun toJson(): JSONObject {
        val obj = JSONObject()
        obj.put("game_id", gameId)
        obj.put("duration_ms", durationMs)
        obj.put("generation_time_ms", generationTimeMs)
        obj.put("coin_count", coinCount)
        obj.put("coin_bins", coinBins)
        obj.put("radius_start", radiusStart)
        obj.put("radius_end", radiusEnd)
        obj.put("velocity_start", velocityStart)
        obj.put("velocity_end", velocityEnd)
        obj.put("velocity_increment", velocityIncrement)
        obj.put("radiusIncrement", radiusIncrement)
        obj.put("seed", seed)
        return obj
    }

    companion object {
        const val NAME = "game_params"

        fun fromStr(body: String): GameParams {
            val obj = JSONObject(body)
            return GameParams(
                gameId = obj.getInt("game_id"),
                durationMs = obj.getInt("duration_ms"),
                generationTimeMs = obj.getInt("generation_time_ms"),
                coinCount = obj.getInt("coin_count"),
                coinBins = obj.getInt("coin_bins"),
                radiusStart = obj.getDouble("radius_start").toFloat(),
                radiusEnd = obj.getDouble("radius_end").toFloat(),
                velocityStart = obj.getDouble("velocity_start").toFloat(),
                velocityEnd = obj.getDouble("velocity_end").toFloat(),
                velocityIncrement = obj.getDouble("velocity_increment").toFloat(),
                radiusIncrement = obj.getDouble("radius_increment").toFloat(),
                seed = obj.getInt("seed")
            )
        }

        fun default(): GameParams {
            return GameParams(
                gameId = 0,
                durationMs = 5 * 1000,
                generationTimeMs = (2.5f * 1000).toInt(),
                coinCount = 20,
                coinBins = 2,
                radiusStart = 50f,
                radiusEnd = 200f,
                velocityStart = 1.7f,
                velocityEnd = 0.6f,
                velocityIncrement = 0.008f,
                radiusIncrement = 0.4f,
                seed = 42
            )
        }
    }
}

class StartGame(
    val ownerId: Int
) : MsgBody {
    override val name: String = NAME

    override fun toJson(): JSONObject {
        val obj = JSONObject()
        obj.put("owner_id", ownerId)
        return obj
    }

    companion object {
        const val NAME = "start_game"
    }
}

class ListCoins(
    val coinIds: IntArray,
    val progress: FloatArray
) : MsgBody {
    override val name: String = NAME

    override fun toJson(): JSONObject {
        val obj = JSONObject()
        val coinsIdsObj = JSONArray()
        val progressObj = JSONArray()
        for (i in coinIds.indices) {
            coinsIdsObj.put(coinIds[i])
            progressObj.put(progress[i])
        }

        obj.put("coin_ids", coinsIdsObj)
        obj.put("progress", progressObj)

        return obj
    }

    companion object {
        const val NAME = "list_coins"

        fun single(coinId: Int, progress: Float): ListCoins {
            return ListCoins(
                coinIds = intArrayOf(coinId),
                progress = floatArrayOf(progress)
            )
        }

        fun fromStr(body: String): ListCoins {
            val obj = JSONObject(body)
            val coinsIdsObj = obj.getJSONArray("coin_ids")
            val progressObj = obj.getJSONArray("progress")

            val coinIds = IntArray(coinsIdsObj.length())
            val progress = FloatArray(coinsIdsObj.length())
            for (i in coinIds.indices) {
                coinIds[i] = coinsIdsObj.getInt(i)
                progress[i] = progressObj.getDouble(i).toFloat()
            }
            return ListCoins(
                coinIds = coinIds,
                progress = progress
            )
        }
    }
}

class FinishGame(
    val gameId: Int,
    val score: Int,
    val count: Int
) : MsgBody {
    override val name: String = NAME

    override fun toJson(): JSONObject {
        val obj = JSONObject()
        obj.put("game_id", gameId)
        obj.put("score", score)
        obj.put("count", count)
        return obj
    }

    companion object {
        const val NAME = "finish_game"

        fun fromJson(body: String) : FinishGame {
            val obj = JSONObject(body)
            return FinishGame(
                gameId = obj.getInt("game_id"),
                score = obj.getInt("score"),
                count = obj.getInt("count")
            );
        }
    }
}