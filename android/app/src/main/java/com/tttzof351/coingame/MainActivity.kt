package com.tttzof351.coingame

import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.util.Log
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import com.tttzof351.coingame.models.*
import com.tttzof351.coingame.render.Coin
import com.tttzof351.coingame.render.CoinGameView

class MainActivity : Activity(), ListenerMsg {
    private var coinGameView: CoinGameView? = null

    private var startGameFrameLayout: FrameLayout? = null
    private var chestImageView: ImageView? = null
    private var rootConstraintLayout: ConstraintLayout? = null

    private var chestDirectAnimation: AnimationDrawable? = null
    private var chestInvertedAnimation: AnimationDrawable? = null

    private var network = Network()

    @Volatile
    var playerId: PlayerId? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        network.attachListener(this)
        network.init(applicationContext)

        startGameFrameLayout = findViewById(R.id.fl_start_game)
        chestImageView = findViewById(R.id.iv_chest)
        rootConstraintLayout = findViewById(R.id.cl_root)

        val (directAnimation, invertedAnimation) = createChestAnimation(applicationContext)
        chestDirectAnimation = directAnimation
        chestInvertedAnimation = invertedAnimation
        chestImageView?.setImageDrawable(chestDirectAnimation)

        startGameFrameLayout?.setOnClickListener { _ ->
            if (coinGameView == null) {
                val id = playerId?.playerId
                if (id != null) {
                    network.sendMsg(StartGame(ownerId = id))
                } else {
                    Toast.makeText(
                        applicationContext,
                        R.string.player_id_error,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                showCloseChestAnimation()
                rejectGameView()
            }
        }
    }

    private fun receiveGameParamsOkHttpHandler(gameParams: GameParams) {
        runOnUiThread {
            showOpenChestAnimation()
            injectGameView(gameParams)
        }
    }

    private fun receiveListCoinsOkHttpHandler(listCoins: ListCoins) {
        runOnUiThread {
            coinGameView?.explode(listCoins)
        }
    }

    private fun receiveFinishGameOkHttpHandler(finishGame: FinishGame) {
        runOnUiThread {
            Toast.makeText(
                applicationContext,
                getString(R.string.score_title, finishGame.score, finishGame.count),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun showCloseChestAnimation() {
        chestInvertedAnimation?.selectDrawable(0)
        chestInvertedAnimation?.stop()
        chestDirectAnimation?.stop()
        chestImageView?.setImageDrawable(chestInvertedAnimation)
        chestInvertedAnimation?.start()
    }

    private fun showOpenChestAnimation() {
        chestDirectAnimation?.selectDrawable(0)
        chestInvertedAnimation?.stop()
        chestDirectAnimation?.stop()
        chestImageView?.setImageDrawable(chestDirectAnimation)
        chestDirectAnimation?.start()
    }

    private fun explosionGlThreadHandler(coin: Coin, progress: Float) {
        runOnUiThread {
            network.sendMsg(
                ListCoins.single(coin.id, progress)
            )
        }
    }

    override fun onDestroy() {
        network.detachListener(this)
        network.close()
        coinGameView?.clean()
        coinGameView = null
        super.onDestroy()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun injectGameView(gameParams: GameParams) {
        if (coinGameView != null) {
            Log.e(MAIN_TAG, "Attempt injection with not null surface")
            return
        }

        if (rootConstraintLayout == null) {
            Log.e(MAIN_TAG, "Can't inject view without root view")
            return
        }

        coinGameView = CoinGameView.create(
            this,
            gameParams,
            chestImageView!!,
            ::explosionGlThreadHandler
        )

        rootConstraintLayout!!.addView(coinGameView)

        rootConstraintLayout!!.postDelayed({
            showCloseChestAnimation()
            rejectGameView()
        }, gameParams.durationMs.toLong())
    }

    private fun rejectGameView() {
        if (coinGameView == null) {
            Log.e(MAIN_TAG, "Attempt reject with null surface")
            return
        }

        coinGameView!!.clean()
        rootConstraintLayout?.removeView(coinGameView)
        coinGameView!!.clean()
        coinGameView = null
    }

    override fun receiveMsg(msg: Msg) {
        when (msg.name) {
            PlayerId.NAME -> {
                playerId = PlayerId.fromStr(msg.body)
            }
            GameParams.NAME -> {
                val gameParams = GameParams.fromStr(msg.body)
                receiveGameParamsOkHttpHandler(gameParams)
            }
            ListCoins.NAME -> {
                val listCoins = ListCoins.fromStr(msg.body)
                receiveListCoinsOkHttpHandler(listCoins)
            }
            FinishGame.NAME -> {
                val finishGame = FinishGame.fromJson(msg.body)
                receiveFinishGameOkHttpHandler(finishGame)
            }
            else -> {}
        }
    }


    companion object {
        const val MAIN_TAG = "MAIN"
    }
}

