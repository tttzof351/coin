package com.tttzof351.coingame.render

import com.tttzof351.coingame.euclidDistance
import com.tttzof351.coingame.models.GameParams
import com.tttzof351.coingame.models.ListCoins
import com.tttzof351.coingame.models.Vec2d
import java.util.*
import java.util.concurrent.ArrayBlockingQueue
import kotlin.collections.ArrayList
import kotlin.math.sqrt
import kotlin.random.Random

class CoinsHolder(
    private val gameParams: GameParams,
    private val explosionHandler: Function2<Coin, Float, Unit>
) {
    val activateCoins: MutableList<Coin?> = ArrayList(gameParams.coinCount)

    private val clickQueue = ArrayBlockingQueue<Vec2d>(100)
    private val netExplosionQueue = ArrayBlockingQueue<ListCoins>(100)

    private var produceStep = 0
    private val poolCoins: LinkedList<Coin> = LinkedList<Coin>()
    private val rand = Random(gameParams.seed)

    private var isFinishPeriod = false
    private var minTimeMs = Float.MIN_VALUE
    private val targetSide: Vec2d = floatArrayOf(0.0f, 0.0f)

    fun updateActiveCoins(
        currentTimeMs: Long
    ) {
        val countStep = gameParams.coinCount / gameParams.coinBins
        val produceStepTime = (gameParams.generationTimeMs / countStep) * produceStep.toFloat()
        val timeLeft = gameParams.durationMs - currentTimeMs

        isFinishPeriod = this.minTimeMs > timeLeft

        if (currentTimeMs > produceStepTime) {
            for (i in 0 until gameParams.coinBins) {
                if (poolCoins.size > 0) {
                    activateCoins.add(poolCoins.pop())
                }
            }
            produceStep += 1
        }
    }

    fun initPoolCoins(
        startPosition: Vec2d
    ) {
        (0 until gameParams.coinCount).forEach { id ->
            poolCoins.add(
                Coin(
                    id = id,
                    pos = startPosition.clone(),
                    velocity = randomVelocity(gameParams),
                    radius = gameParams.radiusStart
                )
            )
        }
    }

    fun updateState(
        currentTimeMs: Long,
        diff: Long,
        width: Int,
        height: Int
    ) {
        val progress = currentTimeMs.toFloat() / gameParams.durationMs.toFloat()

        var click = clickQueue.poll()
        while (click != null) {
            for (i in activateCoins.size - 1 downTo 0) {
                val coin = activateCoins[i] ?: continue
                if (coin.state != CoinState.NORMAL) {
                    continue
                }

                if (euclidDistance(coin.pos, click) < coin.radius) {
                    coin.markExplosion(currentTimeMs)
                    explosionHandler.invoke(coin, progress)
                    break
                }
            }
            click = clickQueue.poll()
        }

        var explosionListCoins = netExplosionQueue.poll()
        while (explosionListCoins != null) {
            for (i in activateCoins.size - 1 downTo 0) {
                val coin = activateCoins[i] ?: continue
                if (coin.state != CoinState.NORMAL) {
                    continue
                }

                for (coinId in explosionListCoins.coinIds) {
                    if (coin.id == coinId) {
                        coin.markExplosion(currentTimeMs)
                    }
                }
            }
            explosionListCoins = netExplosionQueue.poll()
        }

        for (i in 0 until activateCoins.size) {
            val coin = activateCoins[i] ?: continue
            val countQuantums = diff / Coin.QUANTUM_MS
            val remainder = diff % Coin.QUANTUM_MS

            for (j in 0 until countQuantums) {
                if (isFinishPeriod) {
                    coin.checkBound = false
                }
                coin.updateState(
                    gameParams = gameParams,
                    step = Coin.QUANTUM_MS.toFloat(),
                    sceneWidth = width,
                    sceneHeight = height
                )
            }

            coin.updateState(
                gameParams = gameParams,
                step = remainder.toFloat(),
                sceneWidth = width,
                sceneHeight = height
            )

            when (coin.state) {
                CoinState.NORMAL -> {
                    val velocityAmp = euclidDistance(coin.velocity)
                    if (velocityAmp >= gameParams.velocityEnd) {
                        if (coin.velocity[0] > 0) {
                            targetSide[0] = width.toFloat()
                        } else {
                            targetSide[0] = 0f
                        }

                        if (coin.velocity[1] > 0) {
                            targetSide[1] = 0f
                        } else {
                            targetSide[1] = height.toFloat()
                        }
                        val targetMinTimeMs =
                            euclidDistance(coin.pos, targetSide) / velocityAmp
                        if (targetMinTimeMs > this.minTimeMs) {
                            this.minTimeMs = targetMinTimeMs
                        }
                    }
                }
                CoinState.EXPLOSION -> {
                    if (coin.isFinishedExplosion(currentTimeMs)) {
                        activateCoins[i] = null
                    } else {
                        coin.updateExplosionFrame(currentTimeMs)
                    }
                }
            }
        }
    }

    fun click(clickPos: Vec2d) {
        clickQueue.add(clickPos)
    }

    fun explode(listCoins: ListCoins) {
        netExplosionQueue.add(listCoins)
    }

    private fun randomVelocity(gameParams: GameParams): Vec2d {
        var velocityX = 2.0 * rand.nextDouble() - 1.0
        var velocityY = 2.0 * rand.nextDouble() - 1.0

        val length = sqrt(velocityX * velocityX + velocityY * velocityY)
        velocityX /= length
        velocityY /= length

        velocityX *= gameParams.velocityStart
        velocityY *= gameParams.velocityStart

        return floatArrayOf(velocityX.toFloat(), velocityY.toFloat())
    }

    companion object {
        const val TAG = "COIN_HOLDER"
    }
}