package com.tttzof351.coingame.render

import android.content.Context
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.opengl.GLUtils
import android.util.Log
import com.tttzof351.coingame.*
import com.tttzof351.coingame.render.Coin.Companion.EXPLOSION_FRAME_COUNT
import com.tttzof351.coingame.models.GameParams
import com.tttzof351.coingame.models.ListCoins
import com.tttzof351.coingame.models.Vec2d
import java.nio.ByteBuffer
import java.nio.ByteOrder
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10


class CoinRender(
    private val applicationContext: Context,
    private val startPosition: Vec2d,
    private val gameParams: GameParams,
    private val explosionHandler: Function2<Coin, Float, Unit>
) : GLSurfaceView.Renderer {
    private var width = 0
    private var height = 0

    private var textureVertexHandle = 0
    private var textureFragmentHandle = 0
    private var textureProgramHandle = -1
    private val coinTexture = intArrayOf(-1)
    private val explosionTexture = intArrayOf(-1)

    private var triangleVertexHandle = 0
    private var triangleFragmentHandle = 0
    private var triangleProgramHandle = -1

    private val textureVertexSrc = """
        uniform mat4 uScreen;
        attribute vec2 aPosition;
        attribute vec2 aTexPos;
        varying vec2 vTexPos;
        void main() {
            vTexPos = aTexPos;
            gl_Position = uScreen * vec4(aPosition.xy, 0.0, 1.0);
        }
    """
    private val textureFragmentSrc = """
        precision mediump float;
        uniform sampler2D uTexture;
        varying vec2 vTexPos;
        void main(void) {
            gl_FragColor = texture2D(uTexture, vTexPos);
            //gl_FragColor[3] = gl_FragColor[3] * 0.9;
        }
    """

    private val triangleVertexSrc = """
        uniform mat4 uScreen;
        attribute vec2 aPosition;
        void main() {            
            gl_Position = uScreen * vec4(aPosition.xy, 0.0, 1.0);                
        }                                   
    """

    private val triangleFragmentSrc = """
        precision mediump float;
        void main(void) {
            gl_FragColor = vec4(1, 0, 0, 1);
        }
    """

    private val uTextureScreen = floatArrayOf(
        2f / 1080f, 0f, 0f, 0f,
        0f, -2f / 1920f, 0f, 0f,
        0f, 0f, 0f, 0f,
        -1f, 1f, 0f, 1f
    )

    private val uTextureScreenBuffer = ByteBuffer.allocateDirect(
        uTextureScreen.size * SIZE_OF_FLOAT
    ).order(ByteOrder.nativeOrder()).asFloatBuffer()

    private val coinCoordinate = floatArrayOf(
        0f, 0f,      // V1
        0f, 0f,      // UV-V1
        0f, 100f,    // V2
        0f, 1f,      // UV-V2
        100f, 0f,    // V3
        1f, 0f,      // UV-V3
        100f, 100f,  // V4
        1f, 1f       // UV-V4
    )

    private val coinPositionSize = 2
    private val coinUVSize = 2
    private val coinTotalSize = coinPositionSize + coinUVSize
    private val coinPositionOffset = 0
    private val coinTextureOffset = 2
    private val coinCountPoints = coinCoordinate.size / coinTotalSize

    private val coinCoordinateBuffer = ByteBuffer.allocateDirect(
        coinCoordinate.size * SIZE_OF_FLOAT
    ).order(ByteOrder.nativeOrder()).asFloatBuffer()

    private var startTimeMs = 0L
    private var prevFrameTimeMs = 0L
    private var frameIndex = 0L

    private val coinsHolder = CoinsHolder(gameParams, explosionHandler)

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        Log.d(TAG, "onSurfaceCreated")
        startTimeMs = System.currentTimeMillis()
        prevFrameTimeMs = 0L
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        if (this.width == 0 && width > 0) {
            coinsHolder.initPoolCoins(startPosition)
        }

        this.width = width
        this.height = height

        if (coinTexture[0] == -1) {
            loadCoinTexture()
            loadExplosionTexture()
        }

        if (textureProgramHandle == -1) {
            initTextureShaders()
            initTriangleShaders()
        }

        if (width != 0 && height != 0) {
            setupViewport(width, height)
            updateTextureScreen(width, height)
        }

        //activateCoinTexture()
        //activateExplosionTexture()
    }

    override fun onDrawFrame(gl: GL10?) {
        val currentFrameTimeMs = System.currentTimeMillis() - startTimeMs
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT)

        coinsHolder.updateActiveCoins(currentFrameTimeMs)

        for (i in 0 until coinsHolder.activateCoins.size) {
            val coin = coinsHolder.activateCoins[i] ?: continue
            //TODO: See: https://learnopengl.com/Advanced-OpenGL/Instancing
            drawCoin(coin)
        }

        val diff = currentFrameTimeMs - this.prevFrameTimeMs
        coinsHolder.updateState(
            currentFrameTimeMs,
            diff,
            width,
            height
        )

        this.prevFrameTimeMs = currentFrameTimeMs
        this.frameIndex += 1
    }

    fun clean() {
        if (textureProgramHandle != -1) {
            GLES20.glDeleteProgram(textureProgramHandle)
            GLES20.glDeleteShader(textureVertexHandle)
            GLES20.glDeleteShader(textureFragmentHandle)
            GLES20.glDeleteTextures(coinTexture.size, coinTexture, 0) // free the texture!
            GLES20.glDeleteTextures(coinTexture.size, explosionTexture, 0) // free the texture!
        }

        if (triangleProgramHandle != -1) {
            GLES20.glDeleteProgram(triangleProgramHandle)
            GLES20.glDeleteShader(triangleVertexHandle)
            GLES20.glDeleteShader(triangleFragmentHandle)
        }
    }

    fun click(clickPos: Vec2d) {
        coinsHolder.click(clickPos)
    }

    fun explode(listCoins: ListCoins) {
        coinsHolder.explode(listCoins)
    }

    private fun initTextureShaders() {
        Log.d(TAG, "initTextureShaders")
        textureVertexHandle = loadVertexShader(textureVertexSrc)
        textureFragmentHandle = loadFragmentShader(textureFragmentSrc)
        textureProgramHandle = createProgram(textureVertexHandle, textureFragmentHandle)
    }

    private fun initTriangleShaders() {
        Log.d(TAG, "initTriangleShaders")
        triangleVertexHandle = loadVertexShader(triangleVertexSrc)
        triangleFragmentHandle = loadFragmentShader(triangleFragmentSrc)
        triangleProgramHandle = createProgram(triangleVertexHandle, triangleFragmentHandle)
    }

    private fun loadCoinTexture() {
        Log.d(TAG, "loadCoinTexture")
        GLES20.glGenTextures(1, coinTexture, 0)
        if (coinTexture[0] == GLES20.GL_FALSE) {
            throw RuntimeException("Error loading texture")
        }

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, coinTexture[0])
        GLES20.glTexParameteri(
            GLES20.GL_TEXTURE_2D,
            GLES20.GL_TEXTURE_MIN_FILTER,
            GLES20.GL_NEAREST
        )
        GLES20.glTexParameteri(
            GLES20.GL_TEXTURE_2D,
            GLES20.GL_TEXTURE_MAG_FILTER,
            GLES20.GL_NEAREST
        )

        val bitmap = readBitmapFromAssets(applicationContext, "coin/coin.png")!!
        val transparentBitmap = createTransparentBitmap(bitmap, opacity = 200)
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, transparentBitmap, 0)
        bitmap.recycle()
        transparentBitmap.recycle()
    }

    private fun loadExplosionTexture() {
        GLES20.glGenTextures(1, explosionTexture, 0)
        if (explosionTexture[0] == GLES20.GL_FALSE) {
            throw RuntimeException("Error loading texture")
        }

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, explosionTexture[0])
        GLES20.glTexParameteri(
            GLES20.GL_TEXTURE_2D,
            GLES20.GL_TEXTURE_MIN_FILTER,
            GLES20.GL_NEAREST
        )
        GLES20.glTexParameteri(
            GLES20.GL_TEXTURE_2D,
            GLES20.GL_TEXTURE_MAG_FILTER,
            GLES20.GL_NEAREST
        )

        val bitmap = readBitmapFromAssets(applicationContext, "explosion/explosion_spritesheet.png")!!
        val transparentBitmap = createTransparentBitmap(bitmap, opacity = 200)
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, transparentBitmap, 0)
        bitmap.recycle()
        transparentBitmap.recycle()
    }

    private fun setupViewport(width: Int, height: Int) {
        GLES20.glViewport(0, 0, width, height)
        GLES20.glClearColor(0f, 0f, 0f, 0f)

        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)
        GLES20.glEnable(GLES20.GL_BLEND)
    }

    private fun updateTextureScreen(width: Int, height: Int) {
        GLES20.glUseProgram(textureProgramHandle)
        val uScreenPos = GLES20.glGetUniformLocation(textureProgramHandle, "uScreen")
        uTextureScreen[0] = 2f / width.toFloat()
        uTextureScreen[5] = -2f / height.toFloat()
        uTextureScreenBuffer.clear()

        uTextureScreenBuffer
            .put(uTextureScreen)
            .position(0)

        GLES20.glUniformMatrix4fv(
            uScreenPos,
            uTextureScreenBuffer.limit() / uTextureScreen.size,
            false,
            uTextureScreenBuffer
        )
    }

    private fun activateCoinTexture() {
        GLES20.glUseProgram(textureProgramHandle)
        val uTexture = GLES20.glGetUniformLocation(textureProgramHandle, "uTexture")
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, coinTexture[0])
        GLES20.glUniform1i(uTexture, 0)
    }

    private fun activateExplosionTexture() {
        GLES20.glUseProgram(textureProgramHandle)
        val uTexture = GLES20.glGetUniformLocation(textureProgramHandle, "uTexture")
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, explosionTexture[0])
        GLES20.glUniform1i(uTexture, 0)
    }

    private fun drawCoin(
        coin: Coin
    ) {
        GLES20.glUseProgram(textureProgramHandle)
        val aPosition = GLES20.glGetAttribLocation(textureProgramHandle, "aPosition")
        val aTexPos = GLES20.glGetAttribLocation(textureProgramHandle, "aTexPos")

        when (coin.state) {
            CoinState.NORMAL -> {
                activateCoinTexture()
                coinCoordinate[2] = 0f
                coinCoordinate[3] = 0f

                coinCoordinate[6] = 0f
                coinCoordinate[7] = 1f

                coinCoordinate[10] = 1f
                coinCoordinate[11] = 0f

                coinCoordinate[14] = 1f
                coinCoordinate[15] = 1f
            }
            CoinState.EXPLOSION -> {
                activateExplosionTexture()
                val animationFrame = coin.explosionFrame
                val leftU = animationFrame.toFloat() / EXPLOSION_FRAME_COUNT
                val rightU = (animationFrame + 1).toFloat() / EXPLOSION_FRAME_COUNT

                coinCoordinate[2] = leftU
                coinCoordinate[3] = 0f

                coinCoordinate[6] = leftU
                coinCoordinate[7] = 1f

                coinCoordinate[10] = rightU
                coinCoordinate[11] = 0f

                coinCoordinate[14] = rightU
                coinCoordinate[15] = 1f
            }
        }

        coinCoordinate[0] = coin.pos[0] - coin.radius
        coinCoordinate[1] = coin.pos[1] - coin.radius

        coinCoordinate[4] = coin.pos[0] - coin.radius
        coinCoordinate[5] = coin.pos[1] + coin.radius

        coinCoordinate[8] = coin.pos[0] + coin.radius
        coinCoordinate[9] = coin.pos[1] - coin.radius

        coinCoordinate[12] = coin.pos[0] + coin.radius
        coinCoordinate[13] = coin.pos[1] + coin.radius

        coinCoordinateBuffer.clear()
        coinCoordinateBuffer.put(coinCoordinate)

        coinCoordinateBuffer.position(coinPositionOffset)
        GLES20.glVertexAttribPointer(
            aPosition,
            coinPositionSize,
            GLES20.GL_FLOAT,
            false,
            coinTotalSize * SIZE_OF_FLOAT,
            coinCoordinateBuffer
        )
        GLES20.glEnableVertexAttribArray(aPosition)

        coinCoordinateBuffer.position(coinTextureOffset)
        GLES20.glVertexAttribPointer(
            aTexPos,
            coinUVSize,
            GLES20.GL_FLOAT,
            false,
            coinTotalSize * SIZE_OF_FLOAT,
            coinCoordinateBuffer
        )

        GLES20.glEnableVertexAttribArray(aTexPos)
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, coinCountPoints)
    }

    companion object {
        const val TAG = "RENDER"
    }
}