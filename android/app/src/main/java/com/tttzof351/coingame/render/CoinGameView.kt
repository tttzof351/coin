package com.tttzof351.coingame.render

import android.annotation.SuppressLint
import android.content.Context
import android.opengl.GLSurfaceView
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.tttzof351.coingame.MainActivity
import com.tttzof351.coingame.makeTransparent
import com.tttzof351.coingame.models.GameParams
import com.tttzof351.coingame.models.ListCoins

class CoinGameView(context: Context?) : GLSurfaceView(context) {
    private var coinRender: CoinRender? = null

    override fun setRenderer(renderer: Renderer?) {
        super.setRenderer(renderer)
        this.coinRender = renderer as CoinRender
    }

    fun explode(listCoins: ListCoins) {
        coinRender!!.explode(listCoins)
    }

    fun clean() {
        onPause()
        coinRender?.clean()
        coinRender = null
    }

    companion object {
        @SuppressLint("ClickableViewAccessibility")
        fun create(
            context: Context,
            gameParams: GameParams,
            startView: View,
            explosionHandler: Function2<Coin, Float, Unit>,
        ): CoinGameView {
            val coinGameView = CoinGameView(context)
            coinGameView.makeTransparent()

            val buff = IntArray(2)
            startView.getLocationOnScreen(buff)
            Log.d(MainActivity.MAIN_TAG, "position: ${buff[0]}, ${buff[1]}")
            val startPosition = buff.map { it.toFloat() }.toFloatArray()

            val render = CoinRender(
                applicationContext = context.applicationContext,
                startPosition = startPosition,
                gameParams = gameParams,
                explosionHandler = explosionHandler
            )
            coinGameView.setRenderer(render)
            coinGameView.setOnTouchListener { v, event ->
                if (event.action == MotionEvent.ACTION_DOWN) {
                    val clickPos = floatArrayOf(event.x, event.y)
                    Log.d(MainActivity.MAIN_TAG, "add click, (${clickPos[0]}, ${clickPos[1]})")
                    render.click(clickPos)
                }
                true
            }

            return coinGameView
        }
    }

}