package com.tttzof351.coingame

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.tttzof351.coingame.models.Msg
import com.tttzof351.coingame.models.MsgBody
import okhttp3.*
import java.lang.Exception
import java.lang.ref.WeakReference

interface ListenerMsg {
    fun receiveMsg(msg: Msg)
}

class Network() {
    private var webSocket: WebSocket? = null
    private var client: OkHttpClient? = null

    private var listeners: MutableList<WeakReference<ListenerMsg>> = ArrayList()

    fun sendMsg(body: MsgBody) {
        val text = body.toJson().toString()
        val name = body.name

        val isSuccess = webSocket!!.send(
            Msg(
                id = 0,
                name = name,
                body = text
            ).toJson().toString()
        )
        Log.d(SOCKET_TAG, "send: $name $text isSuccess: $isSuccess")
    }

    fun attachListener(listener: ListenerMsg) {
        assertUiThread("Can't attach listener outside ui thread")
        listeners.add(WeakReference(listener))
    }

    fun detachListener(listener: ListenerMsg) {
        assertUiThread("Can't detach listener outside ui thread")
        val iterator = listeners.iterator()
        while (iterator.hasNext()) {
            val it = iterator.next()
            if (listener == it.get()) {
                iterator.remove()
            }
        }
    }

    fun init(applicationContext: Context) {
        if (client == null) {
            client = OkHttpClient()
            val request: Request = Request.Builder().url(REMOTE_ADDRESS).build()

            webSocket = client?.newWebSocket(request, object : WebSocketListener() {
                override fun onOpen(webSocket: WebSocket, response: Response) {
                    super.onOpen(webSocket, response)
                    Log.d(SOCKET_TAG, "open")
                }

                override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
                    super.onFailure(webSocket, t, response)
                    Log.d(SOCKET_TAG, "failure")
                    runOnUiThread {
                        Toast.makeText(
                            applicationContext,
                            R.string.failure_connection_title,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onMessage(webSocket: WebSocket, text: String) {
                    super.onMessage(webSocket, text)
                    Log.d(SOCKET_TAG, "receive: $text")

                    try {
                        val msg = Msg.fromStr(text)
                        for (listener in listeners) {
                            listener.get()?.receiveMsg(msg)
                        }
                    } catch (ex: Exception) {
                        Log.e(SOCKET_TAG, "Error", ex)
                    }
                }
            })
        }
    }

    fun close() {
        webSocket?.close(CLOSE_CODE_OK, null)
        client?.dispatcher?.executorService?.shutdown()

        webSocket = null
        client = null
        listeners.clear()
    }

    companion object {
        const val SOCKET_TAG = "SOCKET"

        const val REMOTE_ADDRESS = "ws://94.103.92.31/game"
        const val LOCALHOST_ADDRESS = "ws://10.0.2.2/game"

        //https://datatracker.ietf.org/doc/html/rfc6455#section-7.4.1
        const val CLOSE_CODE_OK = 1000
    }
}