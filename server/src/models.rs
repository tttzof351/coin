use serde::{Deserialize, Serialize};

pub type UMsgId = u32;
pub type UPlayerId = u32;
pub type UMessageId = u32;
pub type UGameId = u32;
pub type UCoinId = u32;
pub type FClientProgress = f32;


#[derive(Serialize, Deserialize)]
pub struct Msg {
    pub id: UMsgId,
    pub name: String,
    pub body: String,
}

pub trait ToMsg: Serialize {
    type BaseType;
    fn get_name(&self) -> &str;

    fn to_msg(&self, id: UMsgId) -> Option<Msg>
        where Self: Serialize {
        let body = match serde_json::to_string(self) {
            Ok(body) => body,
            Err(_) => return None
        };

        Option::from(Msg::new(
            id,
            self.get_name(),
            body,
        ))
    }
}

impl Msg {
    pub fn new(id: UMsgId, name: &str, body: String) -> Self {
        Msg {
            id: id,
            name: name.to_owned(),
            body: body.to_owned(),
        }
    }

    pub fn to_str(&self) -> Option<String> {
        match serde_json::to_string(self) {
            Ok(str) => Some(str),
            Err(err) => {
                eprintln!("can't call to_str: {}", err);
                None
            }
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct PlayerId {
    player_id: UPlayerId,
}

impl PlayerId {
    pub fn new(player_id: UPlayerId) -> Self {
        PlayerId {
            player_id: player_id
        }
    }
}

impl ToMsg for PlayerId {
    type BaseType = PlayerId;

    fn get_name(&self) -> &str {
        return "player_id";
    }
}


#[derive(Serialize, Deserialize)]
pub struct StartGame {
    owner_id: UPlayerId,
}

impl ToMsg for StartGame {
    type BaseType = StartGame;

    fn get_name(&self) -> &str {
        return "start_game";
    }
}

#[derive(Clone, Copy)]
#[derive(Serialize, Deserialize)]
pub struct GameParams {
    pub game_id: UGameId,
    pub duration_ms: u32,
    pub generation_time_ms: u32,
    pub coin_count: u32,
    pub coin_bins: u32,
    pub radius_start: f32,
    pub radius_end: f32,
    pub velocity_start: f32,
    pub velocity_end: f32,
    pub velocity_increment: f32,
    pub radius_increment: f32,
    pub seed: u16,
}

impl GameParams {
    pub fn new(game_id: UGameId, seed: u16) -> Self {
        GameParams {
            game_id: game_id,
            duration_ms: 15000,
            generation_time_ms: 13500,
            coin_count: 34,
            coin_bins: 2,
            radius_start: 50.0,
            radius_end: 200.0,
            velocity_start: 1.7,
            velocity_end: 0.6,
            velocity_increment: 0.008,
            radius_increment: 0.4,
            seed: seed,
        }
    }
}

impl ToMsg for GameParams {
    type BaseType = GameParams;

    fn get_name(&self) -> &str {
        return "game_params";
    }
}

#[derive(Clone)]
#[derive(Serialize, Deserialize)]
pub struct ListCoins {
    pub coin_ids: Vec<UCoinId>,
    pub progress: Vec<FClientProgress>,
}

impl ListCoins {
    pub fn check(&self) -> bool {
        self.coin_ids.len() == self.progress.len()
    }
}

impl ToMsg for ListCoins {
    type BaseType = ListCoins;

    fn get_name(&self) -> &str {
        return "list_coins";
    }
}

#[derive(Clone)]
#[derive(Serialize, Deserialize)]
pub struct FinishGame {
    pub game_id: UGameId,
    pub score: u32,
    pub count: u32,
}

impl FinishGame {
    pub fn new(
        game_id: UGameId,
        score: u32,
        count: u32,
    ) -> Self {
        FinishGame {
            game_id: game_id,
            score: score,
            count: count,
        }
    }
}

impl ToMsg for FinishGame {
    type BaseType = FinishGame;

    fn get_name(&self) -> &str {
        return "finish_game";
    }
}
