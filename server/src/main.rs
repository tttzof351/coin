// #![deny(warnings)]
use std::collections::HashMap;
use std::sync::{
    atomic::Ordering,
    Arc,
};
use std::sync::atomic::AtomicU32;
use std::env;

use futures_util::{SinkExt, StreamExt, TryFutureExt};
use tokio::sync::{mpsc, RwLock};
use tokio_stream::wrappers::UnboundedReceiverStream;
use warp::ws::{Message, WebSocket};
use warp::Filter;
use std::time::Duration;
use tokio::time::sleep;
use rand::Rng;
use tokio::sync::mpsc::error::SendError;

mod models;

use models::*;

static NEXT_MESSAGE_ID: AtomicU32 = AtomicU32::new(1);
static NEXT_PLAYER_ID: AtomicU32 = AtomicU32::new(1);

const NON_GAME_ID: UGameId = 0;
static NEXT_GAME_ID: AtomicU32 = AtomicU32::new(1);

fn next_player_id() -> UPlayerId {
    NEXT_PLAYER_ID.fetch_add(1, Ordering::Relaxed)
}

fn next_msg_id() -> UMessageId {
    NEXT_MESSAGE_ID.fetch_add(1, Ordering::Relaxed)
}

fn next_game_id() -> UGameId {
    NEXT_GAME_ID.fetch_add(1, Ordering::Relaxed)
}


#[derive(Clone)]
pub struct Session {
    game_id: UGameId,
    sender: mpsc::UnboundedSender<Message>,
}

impl Session {
    fn send<T: ToMsg>(&self, player_id: &UPlayerId, msg: &T) -> Result<(), SendError<Message>> {
        let text = match msg.to_msg(next_msg_id()).and_then(|msg| msg.to_str()) {
            Some(text) => text,
            None => {
                eprintln!("can't send message with: {}", msg.get_name());
                return Result::Err(SendError::<Message>(Message::text(
                    format!("fake error message for {}", msg.get_name())
                )));
            }
        };
        println!("send: player_id: {}, text: {}", player_id, text);
        return self.sender.send(Message::text(text));
    }
}

type Actions = HashMap<UPlayerId, Vec<(UCoinId, FClientProgress)>>;

#[derive(Clone)]
#[derive(Default)]
pub struct SyncState {
    connection: HashMap<UPlayerId, Session>,
    scores: HashMap<UGameId, Actions>,
    params: HashMap<UGameId, GameParams>,
}

impl SyncState {
    pub fn new() -> Self {
        SyncState {
            connection: HashMap::new(),
            scores: HashMap::new(),
            params: HashMap::new(),
        }
    }

    pub fn start_session(
        &mut self,
        player_id: UPlayerId,
        sender: mpsc::UnboundedSender<Message>,
    ) {
        self.connection.insert(
            player_id,
            Session {
                game_id: NON_GAME_ID,
                sender: sender,
            },
        );
    }

    pub fn change_game_id(&mut self, old_game_id: UGameId, game_id: UGameId) {
        let player_ids: Vec<UPlayerId> = self.connection.keys().cloned().collect();
        for player_id in player_ids {
            match self.connection.get_mut(&player_id) {
                Some(session) => {
                    if session.game_id == old_game_id {
                        session.game_id = game_id
                    }
                }
                None => ()
            }
        }
    }

    pub fn put_list_coins(
        &mut self,
        game_id: UGameId,
        player_id: UPlayerId,
        list_coins: &ListCoins,
    ) {
        let mut coins: Vec<(UCoinId, FClientProgress)> = list_coins.coin_ids
            .iter()
            .cloned()
            .zip(
                list_coins.progress
                    .iter()
                    .cloned()
            ).collect();

        if !self.scores.contains_key(&game_id) {
            self.scores.insert(game_id, Actions::new());
        }

        let actions = match self.scores.get_mut(&game_id) {
            Some(actions) => actions,
            None => {
                eprintln!("can't put list coins without actions! game_id: {}", game_id);
                return;
            }
        };

        if !actions.contains_key(&player_id) {
            actions.insert(player_id, Vec::new());
        }

        match actions.get_mut(&player_id) {
            Some(vec) => {
                vec.append(&mut coins);
            }
            None => {
                eprintln!("can't put coin in actions! player_id: {}", player_id);
                return;
            }
        }
    }

    pub fn remove_scores(&mut self, game_id: UGameId) {
        self.scores.remove(&game_id);
    }

    pub fn print_game_ids(&self) {
        for (&player_id, session) in self.connection.iter() {
            println!("player_id: {}, game_id: {}", player_id, session.game_id);
        }
    }
}

type State = Arc<RwLock<SyncState>>;

/*
    ws://127.0.0.1/game
    {"id":0,"name":"start_game","body": "{\"owner_id\":0}" }
    {"id":0,"name":"list_coins","body": "{\"coin_ids\": [0, 2, 10], \"progress\": [0.01, 0.02, 0.3]}"}
*/

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    let state = State::default();
    let state = warp::any().map(move || state.clone());

    let server = warp::path("game")
        // The `ws()` filter will prepare Websocket handshake...
        .and(warp::ws())
        .and(state)
        .map(|ws: warp::ws::Ws, state| {
            // This will call our function if the handshake succeeds.
            ws.on_upgrade(move |socket| user_connected(socket, state))
        });

    let args: Vec<String> = env::args().collect();
    println!("args: {:?}", args);
    if args.len() < 2 {
        panic!("You must pass ipv4 address for server, like: 127.0.0.1 !")
    }
    let address = args.get(1).unwrap();
    let vec: Vec<&str> = address.split(".").collect();

    if vec.len() < 4 {
        panic!("Incorrect ipv4 address {}", address);
    }

    warp::serve(server).run(([
                                 vec[0].parse::<u8>().unwrap(),
                                 vec[1].parse::<u8>().unwrap(),
                                 vec[2].parse::<u8>().unwrap(),
                                 vec[3].parse::<u8>().unwrap()
                             ], 80)).await;
}

async fn user_connected(socket: WebSocket, state: State) {
    let player_id: UPlayerId = next_player_id();
    eprintln!("connect new player!: {}", player_id);

    let (mut user_ws_tx, mut user_ws_rx) = socket.split();

    //  user_ws_tx.send(
    //      Message::text(
    //          PlayerId::new(player_id).to_msg(next_msg_id()).to_str()
    //      )
    //  ).unwrap_or_else(|e| {
    //     eprintln!("websocket send error: {}", e);
    // })
    // .await;

    let (tx, rx) = mpsc::unbounded_channel();
    let mut rx = UnboundedReceiverStream::new(rx);

    tokio::task::spawn(async move {
        while let Some(message) = rx.next().await {
            user_ws_tx
                .send(message)
                .unwrap_or_else(|e| {
                    eprintln!("websocket send error: {}", e);
                })
                .await;
        }
    });

    state.write().await.start_session(player_id, tx);
    let err = state
        .read()
        .await
        .connection[&player_id]
        .send(&player_id, &PlayerId::new(player_id));

    match err {
        Err(e) => {
            eprintln!("error send player_id: {}", e)
        }
        _ => {}
    }

    while let Some(result) = user_ws_rx.next().await {
        let msg = match result {
            Ok(msg) => msg,
            Err(e) => {
                eprintln!("websocket error(uid={}): {}", player_id, e);
                break;
            }
        };

        user_request(player_id, msg, &state).await;
    }
    user_disconnected(player_id, &state).await;
}

async fn user_request(player_id: UPlayerId, raw_msg: Message, state: &State) {
    let text = match raw_msg.to_str() {
        Ok(str) => str,
        Err(_) => {
            eprintln!("error user request");
            return;
        }
    };

    println!("receive message: player_id: {}, text: {}", player_id, text);

    let msg: Msg = match serde_json::from_str(text) {
        Ok(msg) => msg,
        Err(err) => {
            eprintln!("error parse msg: {}", err);
            return;
        }
    };

    match msg.name.as_ref() {
        "start_game" => {
            //let start_game: StartGame = serde_json::from_str(&msg.body).unwrap();
            handle_start_game(state).await;
        }
        "list_coins" => {
            let list_coins: ListCoins = match serde_json::from_str(&msg.body) {
                Ok(obj) => obj,
                Err(err) => {
                    eprintln!("Error parse list coins {}", err);
                    return;
                }
            };
            handle_list_coins(player_id, &list_coins, state).await;
        }
        _ => ()
    }
}

async fn user_disconnected(player_id: UPlayerId, state: &State) {
    eprintln!("close connection with player: {}", player_id);

    // Stream closed up, so remove from the user list
    state.write().await.connection.remove(&player_id);
}


async fn handle_start_game(state: &State) {
    let game_id: UGameId = next_game_id();
    let game_params = GameParams::new(game_id, rand::thread_rng().gen());

    state.write().await.params.insert(game_id, game_params);
    for (player_id, session) in state.read().await.connection.iter() {
        if session.game_id == NON_GAME_ID {
            if let Err(_disconnected) = session.send(player_id, &game_params) {
                //pass                
            }
        }
    }

    state.write().await.change_game_id(NON_GAME_ID, game_id);
    state.read().await.print_game_ids();

    let (tx, rx) = mpsc::unbounded_channel();
    let mut rx = UnboundedReceiverStream::new(rx);
    if let Err(_disconnected) = tx.send(state.clone()) {
        //pass
    }

    tokio::task::spawn(async move {
        sleep(Duration::from_millis(game_params.duration_ms as u64)).await;
        while let Some(copy_state) = rx.next().await {
            finish_game(game_id, &copy_state).await;
        }
    });
}

async fn finish_game(game_id: UGameId, state: &State) {
    let read_state = state.read().await;
    let scores: &HashMap<UPlayerId, Vec<(UCoinId, FClientProgress)>> = match read_state.scores.get(&game_id) {
        Some(obj) => obj,
        None => {
            eprintln!("can't read scores!");
            drop(read_state);
            clean_game_state(game_id, state).await;
            return;
        }
    };

    let mut scores_per_coin: HashMap<UCoinId, (UPlayerId, FClientProgress)> = HashMap::new();
    let mut scores_per_player: HashMap<UPlayerId, u32> = HashMap::new();

    for (&player_id, coin_progress_vec) in scores.iter() {
        for (coin_id, progress) in coin_progress_vec.iter() {
            match scores_per_coin.get_mut(coin_id) {
                Some((_, old_client_progress)) => {
                    if progress < old_client_progress {
                        scores_per_coin.insert(coin_id.clone(), (player_id, progress.clone()));
                    }
                }
                None => {
                    scores_per_coin.insert(coin_id.clone(), (player_id, progress.clone()));
                }
            }
        }
    }

    for (_, (player_id, _)) in scores_per_coin.iter() {
        let current_count: u32 = scores_per_player.get(player_id).get_or_insert(&0).clone();
        scores_per_player.insert(player_id.clone(), current_count + 1);
    }

    let game_params = match read_state.params.get(&game_id) {
        Some(params) => params,
        None => {
            eprintln!("can't finish game without game_params!, game_id: {}", game_id);
            return;
        }
    };

    let coin_count = game_params.coin_count;

    drop(read_state);

    for (player_id, session) in state.read().await.connection.iter() {
        if session.game_id == game_id {
            let score = scores_per_player.get(player_id).get_or_insert(&0).clone();

            let finish_game: FinishGame = FinishGame::new(
                game_id,
                score,
                coin_count,
            );
            let msg = finish_game;

            if let Err(_disconnected) = session.send(player_id, &msg) {
                //pass                
            }
        }
    }

    clean_game_state(game_id, state).await;
}

async fn clean_game_state(game_id: UGameId, state: &State) {
    state.write().await.change_game_id(game_id, NON_GAME_ID);
    state.read().await.print_game_ids();
    state.write().await.remove_scores(game_id);
}

async fn handle_list_coins(
    player_id: UPlayerId,
    list_coins: &ListCoins,
    state: &State,
) {
    let game_id: UGameId = match state.read().await.connection.get(&player_id) {
        Some(session) => session.game_id,
        None => {
            eprintln!("can't handle list coins without session!, player_id: {}", player_id);
            return;
        }
    };

    state.write().await.put_list_coins(
        game_id,
        player_id,
        list_coins,
    );

    for (&iter_player_id, session) in state.read().await.connection.iter() {
        if session.game_id == game_id && iter_player_id != player_id {
            if let Err(_disconnected) = session.send(&iter_player_id, list_coins) {
                //pass                
            }
        }
    }
}
